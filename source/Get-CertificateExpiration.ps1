
<#PSScriptInfo

.VERSION 0.1.0

.GUID b56d5485-a28f-47a1-a2dc-a49944c520d3

.AUTHOR Michael T Lombardi

.COMPANYNAME STLPSUG

.COPYRIGHT ?

.TAGS example web ssl automation insight certificate

.LICENSEURI 

.PROJECTURI https://gitlab.com/michaeltlombardi/Get-CertificateExpiration

.ICONURI 

.EXTERNALMODULEDEPENDENCIES 

.REQUIREDSCRIPTS 

.EXTERNALSCRIPTDEPENDENCIES 

.RELEASENOTES


#>

<# 
    .SYNOPSIS
     Retrieve certificates and expiration
    .DESCRIPTION 
     Retrieve certificates from the local store and how many days until they expire 
    .EXAMPLE
     Get-CertificateExpiration.ps1

     This example will retreive all certs in the LocalMachine's personal
     certificate store and include the expiration date and days to expiration
     in addition to thumbprint, subject, and issuer information.
    .EXAMPLE
     Get-CertificateExpiration.ps1 -Days 30

     This examle will retrieve all certs in the LocalMachine's personal
     certificate store which will have expired by thirty days from now/
     It will include the expiration date and days to expiration in addition
     to thumbprint, subject, and issuer information.
#>
[cmdletbinding()]
Param(
    [int]$Days
)

# Check _only_ the LocalMachine's Personal store for certificates
Get-ChildItem -Path 'Cert:\LocalMachine\My' |
    Select-Object -Property Thumbprint,
                            Subject,
                            Issuer, 
                            @{
                                Name = 'ExpirationDate'
                                Expression = { $_.NotAfter }
                            },
                            @{
                                Name = 'DaysToExpiration'
                                Expression = {
                                    # We subtract todays date from the expiration date,
                                    # then make sure we return only the whole days until
                                    # expiration.
                                    ($_.NotAfter - (get-date)).Days
                                }
                            } |
        Where-Object -FilterScript {
            # If no days are specified, OR if the cert expires before that
            # many days pass, return the object.
            ! $PSBoundParameters.ContainsKey('Days') -or 
            ($_.ExpirationDate.AddDays(-$Days) -lt (Get-Date))
        }
