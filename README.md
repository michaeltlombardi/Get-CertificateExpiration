# Get-CertificateExpiration
_Retrieve certificates from the LocalMachine cert store and specify their expiration information; optionally only retrieve certificates which will expire within the next N days._

## Install
List of instructions for getting the project locally for use.

## Use
Quick example of something useful you can do with the project, including code.
