$SystemUnderTest = Resolve-Path "$(Split-Path $PSScriptRoot -Parent)/source/Get-CertificateExpiration.ps1"
Describe "Stuff" {
    Context "The cert expires in 28 days" {
        Mock -CommandName Get-ChildItem -MockWith {
            [pscustomobject]@{
                Issuer     = 'Biscuit'
                Subject    = 'Biscuit'
                Thumbprint = 'Biscuit'
                NotAfter   = (Get-Date).AddDays(28)
            }
        }

        It "Returns the cert if Days is not specified" {
            & $SystemUnderTest | Should Not BeNullOrEmpty

        }
        It "Returns the cert if 30 days is specified" {
            & $SystemUnderTest -Days 30 | Should Not BeNullOrEmpty
        }
        It "Returns nothing if 15 days is specified" {
            & $SystemUnderTest -Days 15 | Should BeNullOrEmpty
        }
    }
    
}